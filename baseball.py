import re
import sys

def getname(test):
    hooray=re.compile(r"\w+ \w+ batted\b")
    hooray2=re.compile(r"\w+ \w+")
    match=hooray.search(test)
    if match is not None:
        matcha=hooray2.search(match.group(0))
        if matcha is not None:
            return matcha.group(0)
        else:
            return False
    else:
        return False

def getbats(test):
    hooray=re.compile(r"\d+ times\b")
    match=hooray.search(test)
    hooray2=re.compile(r"\d+")
    if match is not None:
        matcha=hooray2.search(match.group(0))
        return matcha.group(0)
    else:
        return False

def gethits(test):
    hooray=re.compile(r"\d+ hits\b")
    match=hooray.search(test)
    hooray2=re.compile(r"\d+")
    if match is not None:
        matcha=hooray2.search(match.group(0))
        return matcha.group(0)
    else:
        return False

if len(sys.argv)>1:

    filechoose=str(sys.argv[1])+'.txt'
    print(filechoose)
    filey=open(filechoose)

    lines=filey.readlines()
    player_list=[]
    batting_infosum=[]
    player_timesum=[]
    for x in xrange(0,len(lines)):
        playa=getname(lines[x])
        if playa in player_list:
            playid=player_list.index(playa)
            batting_infosum[playid]=batting_infosum[playid]+int(gethits(lines[x]))
            player_timesum[playid]=player_timesum[playid]+int(getbats(lines[x]))
        elif playa is False:
            number=0
        else:
            player_list.append(playa)
            batting_infosum.append(int(gethits(lines[x])))
            player_timesum.append(int(getbats(lines[x])))

    batav=[]
    for x in xrange(0, len(player_list)):
        batav.append(float(batting_infosum[x])/float(player_timesum[x]))
   
    connecta=zip(batav, player_list)
    connecta.sort(reverse=True)
    for x in xrange(0, len(player_list)):
        print(connecta[x][1]), 
        print(": "), 
        print("%.3f" % connecta[x][0])

else:
    print("No file given! Please insert the NAME of a text file (do not include .txt)")
    
