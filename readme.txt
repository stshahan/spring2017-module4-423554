Regular expressions were tested in scripttry.py, and were:
1. "\bhello world\b"
2. "\w*[aeiou]{3}\w*"
3. "\b[A-Z][A-Z]\d\d\d\d?\b"

To test the baseball stats, run:
python baseball.py cardinals-1940
DO NOT add a .txt. This is added at a later step. If you do not add a file name it will prompt you. An error will run if you use an invalid file name.

