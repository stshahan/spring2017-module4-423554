import re

mathelo = re.compile(r"\bhello world\b")

def looking_for(test):
    match = mathelo.search(test)
    if match is not None:
        print("here")
        return match.group(0)
    else:
        return False

print(looking_for("Programmers will often write hello world as their first project with a programming language."))

matrip=re.compile(r"\w*[aeiou]{3}\w*")

def triplevowel(test):
    return matrip.findall(test)

print(triplevowel("The gooey peanut butter and jelly sandwich was a beauty."))

matflight=re.compile(r"\b[A-Z][A-Z]\d\d\d\d?\b")

def flightfind(test):
    return matflight.findall(test)

print(flightfind("AA312 AA1298 NW1234 US443 US31344"))



